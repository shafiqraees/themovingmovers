
<div class="sidebar" data-color="white" data-active-color="blue">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color=" default | primary | info | success | warning | danger |"
  -->
    <div class="logo">
        <a href="https://www.themovingmovers.com" class="simple-text logo-mini">
            <div class="logo-image-small">
                <img src="{{asset('/assets/img/tmmlogo.png')}}">
            </div>
            <!-- <p>CT</p> -->
        </a>
        <a href="#" class="simple-text logo-normal">
            The Moving Movers
            <!-- <div class="logo-image-big">
              <img src="../assets/img/logo-big.png">
            </div> -->
        </a>
    </div>
    <div class="sidebar-wrapper">
        <div class="user">
            <div class="photo">
                <img src="{{asset('/assets/img/faces/ayo-ogunseinde-2.jpg')}}" />
            </div>
            <div class="info">
                <a data-toggle="collapse" href="#collapseExample" class="collapsed">
              <span>
                Admin
                <b class="caret"></b>
              </span>
                </a>
                <div class="clearfix"></div>
                <div class="collapse" id="collapseExample">
                    <ul class="nav">
                        {{--<li>
                            <a href="#">
                                <span class="sidebar-mini-icon">MP</span>
                                <span class="sidebar-normal">My Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">EP</span>
                                <span class="sidebar-normal">Edit Profile</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="sidebar-mini-icon">S</span>
                                <span class="sidebar-normal">Settings</span>
                            </a>
                        </li>--}}
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="dropdown-item">
                                <span class="sidebar-mini-icon">Lo</span>
                                <span class="sidebar-normal">Logout</span>
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <ul class="nav">
            <li class="active">
                <a href="{{ url('admin') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li>
                <a href="{{ url('admin/order') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p> Open Orders </p>
                </a>
            </li>
            {{--<li>
                <a href="{{ url('admin/testOrder') }}">
                    <i class="nc-icon nc-bank"></i>
                    <p> Test Orders </p>
                </a>
            </li>--}}
            <li>
                <a data-toggle="collapse" href="#pagesExamples">
                    <i class="nc-icon nc-book-bookmark"></i>
                    <p>
                        Orders <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse " id="pagesExamples">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('admin/addOrder') }}">
                                <span class="sidebar-mini-icon">AO</span>
                                <span class="sidebar-normal"> Add Order </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/history') }}">
                                <span class="sidebar-mini-icon">OH</span>
                                <span class="sidebar-normal"> Order History </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#componentsExamples">
                    <i class="nc-icon nc-layout-11"></i>
                    <p>
                        Jobs <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse " id="componentsExamples">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('admin/jobOffer') }}">
                                <span class="sidebar-mini-icon">JR</span>
                                <span class="sidebar-normal"> Job Requests </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#formsExamples">
                    <i class="nc-icon nc-ruler-pencil"></i>
                    <p>
                        Packages <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse " id="formsExamples">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('admin/addPackage') }}">
                                <span class="sidebar-mini-icon">AP</span>
                                <span class="sidebar-normal"> Add Package </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/package') }}">
                                <span class="sidebar-mini-icon">PL</span>
                                <span class="sidebar-normal"> Package List </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a data-toggle="collapse" href="#tablesExamples">
                    <i class="nc-icon nc-circle-10"></i>
                    <p>
                        Users <b class="caret"></b>
                    </p>
                </a>
                <div class="collapse " id="tablesExamples">
                    <ul class="nav">
                        <li>
                            <a href="{{ url('admin/addEmployee') }}">
                                <span class="sidebar-mini-icon">AE</span>
                                <span class="sidebar-normal"> Add User </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('admin/employee') }}">
                                <span class="sidebar-mini-icon">EL</span>
                                <span class="sidebar-normal"> User List </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <a href="{{ url('chatify') }}">
                    <i class="nc-icon nc-chat-33"></i>
                    <p> Messages </p>
                </a>
            </li>
        </ul>
    </div>
</div>
