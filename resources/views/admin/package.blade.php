@extends('layouts.admin.app')

@section('title','Package List')
@section('heading','Package')

@section('content')
    <div class="col-md-12">
        <div class="card">{{--
            <div class="card-header">
                <h4 class="card-title"> Package List</h4>
            </div>--}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                Package Name
                            </th>
                            <th>
                                Price
                            </th>
                            <th>
                                No. of Movers
                            </th>
                            <th>
                                Trucks
                            </th>
                            <th>
                                Description
                            </th>
                        </tr></thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Economic Package</td>
                            <td>$75</td>
                            <td>2</td>
                            <td>0</td>
                            <td>EP Details</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Standard Package</td>
                            <td>$100</td>
                            <td>2</td>
                            <td>1</td>
                            <td>SP Details</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Premium Package</td>
                            <td>$150</td>
                            <td>3</td>
                            <td>1</td>
                            <td>PP Details</td>
                        </tr>
                        {{--@foreach($packages as $package)
                            <tr>
                                <td>{{ $package->id}}</td>
                                <td>{{ $package->name}}</td>
                                <td>${{ $package->price}}/hr</td>
                                <td>{{ $package->movers}}</td>
                                <td>{{ $package->truck}}</td>
                                <td>{{ $package->description}}</td>
                            </tr>
                        @endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
