<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css' />
{{--@extends('layouts.admin.app')

@section('title','Order List')
@section('heading','Orders')

@section('content')
@endsection--}}
<div id='fullCalendar'></div>

<script src="{{ asset('/assets/js/core/jquery.min.js')}}"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js'></script>
<script>
   /* var newJQuery = jQuery.noConflict(true);*/

    $(document).ready(function() {
        // page is now ready, initialize the calendar...
        $('#fullCalendar').fullCalendar({
            // put your options and callbacks here
            defaultView: 'agendaWeek',
            events : [
                    @foreach($orders as $order)
                {
                    title : '{{ $order->title }}',
                    start : '{{ $order->start }}',
                    end: '{{ $order->end }}',
                    eventBackgroundColor: '{{$order->address}}'
                },
                @endforeach
            ],
        });
    });
</script>
