@extends('layouts.admin.app')

@section('title','Add Package')
@section('heading','Package')

@section('content')
    <div class="col-md-12">
        <div class="card ">{{--
            <div class="card-header ">
                <h4 class="card-title">Add Package</h4>
            </div>--}}
            <div class="card-body ">
               {{-- <form method="post" action="{{route('package.store')}}" class="form-horizontal">--}}
                <form class="form-horizontal">
                   {{-- @csrf--}}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Name</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="name">
                                <span class="form-text">Enter a package name..</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Price</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="number" class="form-control" placeholder="$" min="0" name="price">
                                <span class="form-text">Enter price per hour.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Movers</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="number" class="form-control" name="movers">
                                <span class="form-text">Enter number of movers.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Trucks</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="number" class="form-control" name="truck">
                                <span class="form-text">Enter number of trucks.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Description</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <textarea class="form-control" name="description">  </textarea>
                                <span class="form-text">Enter Package Description.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-primary">Add</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
