@extends('layouts.admin.app')

@section('title','Users')
@section('heading','User List')

@section('content')
    <div class="col-md-12">
        <div class="card">{{--
            <div class="card-header">
                <h4 class="card-title"> Employee List</h4>
            </div>--}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                User Name
                            </th>
                            <th>
                                Email
                            </th><th>
                                Store
                            </th>
                            <th>
                                Info
                            </th>
                            <th>
                                Type
                            </th>
                        </tr></thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>John</td>
                            <td>John@user.com</td>
                            <td>Some store </td>
                            <td>Some Details</td>
                            <td>Employee</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>User</td>
                            <td>test@user.com</td>
                            <td>Some store </td>
                            <td>Some Details</td>
                            <td>Employee</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>User 2</td>
                            <td>test2@user.com</td>
                            <td>Some store2 </td>
                            <td>Some Details</td>
                            <td>Contractor</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Test</td>
                            <td>test@test.com</td>
                            <td>Some test store </td>
                            <td>Some Details</td>
                            <td>Employee</td>
                        </tr>
                       {{-- @foreach($employees as $employee)
                            <tr>
                                <td>{{ $employee->id}}</td>
                                <td>{{ $employee->first_name}} {{ $employee->last_name}}</td>
                                <td>{{ $employee->email}}</td>
                                <td>{{ $employee->store}}</td>
                                <td>{{ $employee->store_info}}</td>
                            </tr>
                        @endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
