@extends('layouts.admin.app')

@section('title','Orders History')
@section('heading','Orders History')

@section('content')
    <div class="col-md-12">
        <div class="card">{{--
            <div class="card-header">
                <h4 class="card-title"> Order List</h4>
            </div>--}}
            <div class="card-body">
                <div class="table-responsive">
                    {{--<table class="table table-bordered yajra-datatable" id="orderHistory">--}}
                    <table class="table table-bordered yajra-datatable">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                Title
                            </th>
                            <th>
                                Package Name
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                StartTime
                            </th>
                            <th>
                                EndTime
                            </th>
                            <th>
                                Address
                            </th>
                        </tr></thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>Move Furniture</td>
                            <td>Premium Service</td>
                            <td>01/01/2020 </td>
                            <td>10 AM</td>
                            <td>1 PM</td>
                            <td>Some Address</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Move This</td>
                            <td>Standard Service</td>
                            <td>01/01/2020 </td>
                            <td>5 PM</td>
                            <td>7 PM</td>
                            <td>Some Address</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Move That</td>
                            <td>Premium Service</td>
                            <td>01/01/2020 </td>
                            <td>6 PM</td>
                            <td>8 PM</td>
                            <td>Some Address</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>Some Title</td>
                            <td>Economic Service</td>
                            <td>01/01/2020 </td>
                            <td>10 AM</td>
                            <td>12 PM</td>
                            <td>Some Address</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
   {{-- {{ $orders->links() }}--}}
@endsection
