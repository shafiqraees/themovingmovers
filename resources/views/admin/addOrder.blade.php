@extends('layouts.admin.app')

@section('title','Add Order')
@section('heading','Add Order')

@section('content')
<div class="col-md-12">
    <div class="card ">{{--
        <div class="card-header ">
            <h4 class="card-title">Add Order</h4>
        </div>--}}
        <div class="card-body ">
           {{-- <form method="post" action="{{route('order.store')}}" class="form-horizontal">--}}
            <form class="form-horizontal">
               {{-- @csrf--}}
                <div class="row">
                    <label class="col-sm-2 col-form-label">Package</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <select class="form-control" name="package">
                                <option selected>Select a Package</option>
                                @foreach ($packages as $package)
                                    <option value={{$package->id}}>{{$package->name}}</option>
                                @endforeach
                            </select>
                            <span class="form-text">Select a Package.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Title</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="title">
                            <span class="form-text">Enter Title.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Date</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="date" class="form-control" name="date">
                            <span class="form-text">Select Date.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Start Time</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="time" class="form-control" name="startTime">
                            <span class="form-text">Select Starting Time.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">End Time</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="time" class="form-control" name="endTime">
                            <span class="form-text">Select End Time.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">Address</label>
                    <div class="col-sm-10">
                        <div class="form-group">
                            <input type="text" class="form-control" name="address">
                            <span class="form-text">Enter address.</span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6 text-right">
                        <button type="submit" class="btn btn-primary">Add</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
