@extends('layouts.admin.app')

@section('title','Add User')
@section('heading','Add User')

@section('content')
    <div class="col-md-12">
        <div class="card ">
            <div class="card-body ">
                {{--<form method="post" action="{{route('employee.store')}}" class="form-horizontal">--}}
                <form  class="form-horizontal">
                   {{-- @csrf--}}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">First Name</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="first_name">
                                <span class="form-text">Enter First Name..</span>
                            </div>
                        </div>
                        <label class="col-sm-2 col-form-label">Last Name</label>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <input type="text" class="form-control" name="last_name">
                                <span class="form-text">Enter Last Name..</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Type</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <select  class="form-control" name="type">
                                    <option>Employee</option>
                                    <option>Contractor</option>
                                </select>
                                <span class="form-text">Select User Type..</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Email</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="email" class="form-control" name="email">
                                <span class="form-text">Enter email..</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Store</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="store">
                                <span class="form-text">Enter store name.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Store Info</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="store_info">
                                <span class="form-text">Enter store info.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Password</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="password">
                                <span class="form-text">Enter password.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-6 text-right">
                            <button type="submit" class="btn btn-primary">Add</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
