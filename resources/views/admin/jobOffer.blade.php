@extends('layouts.admin.app')

@section('title','Job Requests')
@section('heading','Jobs')

@section('content')
    <div class="col-md-12">
        <div class="card">{{--
            <div class="card-header">
                <h4 class="card-title"> Job Requests</h4>
            </div>--}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                Employee
                            </th>
                            <th>
                                Company Name
                            </th>
                            <th>
                                Order#
                            </th>
                            <th>
                                Package
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Address
                            </th>
                            <th class="text-right">
                                Actions
                            </th>
                        </tr></thead>
                        <tbody>
                        @foreach($jobs as $job)
                            <tr>
                                <td>{{ $job->id}}</td>
                                <td>{{ $job->status}}</td>
                                <td>{{ $job->user->first_name}} {{ $job->user->last_name}}</td>
                                <td>{{ $job->user->store}}</td>
                                <td>{{ $job->order->id}}</td>
                                <td>{{ $job->order->package->name}}</td>
                                <td>{{ $job->order->date}}</td>
                                <td>{{ $job->order->address}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
