@extends('layouts.admin.app')

@section('title','Order List')
@section('heading','Orders')

@section('content')
    <div class="col-md-12">
        <div class="card">
            <style>/* basic positioning */
                .legend { list-style: none; }
                .legend li { float: left; margin-right: 10px; }
                .legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin: 2px; }
                /* your colors */
                .legend .superawesome { background-color: #B9F0B6; }
                .legend .awesome { background-color: #F7D321; }
                .legend .kindaawesome { background-color: #FFE8C2; }
                .legend .notawesome { background-color: #FFC2C2; }</style>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <ul class="legend">
                            <li><span class="superawesome"></span>Open</li>
                            <li><span class="awesome"></span> Assigned</li>
                            <li><span class="kindaawesome"></span> Completed</li>
                            <li><span class="notawesome"></span> Cancelled</li>
                        </ul>
                        
                    </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="fullCalendar"></div>
                            <div id="data" style="display: none">{{$data}}</div>
                            <div id="packages" style="display: none">{$packages}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<div class="modal fade" id="addOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form method="post" action="{{route('order.store')}}" class="form-horizontal">
                    @csrf
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Package</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control" name="package">
                                    <option selected>Select a Package</option>
                                    @foreach ($packages as $package)
                                        <option value={{$package->id}}>{{$package->name}}</option>
                                    @endforeach
                                </select>
                                <span class="form-text">Select a Package.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="title">
                                <span class="form-text">Enter Title.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Date</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input readonly type="text" class="form-control" id="id" name="date">
                                <span class="form-text">Select Date.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Start Time</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="time" class="form-control" name="startTime">
                                <span class="form-text">Select Starting Time.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">End Time</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="time" class="form-control" name="endTime">
                                <span class="form-text">Select End Time.</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <input type="text" class="form-control" name="address">
                                <span class="form-text">Enter address.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editOrder" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Manage Order</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="form-horizontal">
                   {{-- @csrf--}}
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Package</label>
                        <div class="col-sm-10 col-form-label">
                            <div class="form-group">
                                <label class="pull-left">Premium Package</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Title</label>
                        <div class="col-sm-10 col-form-label">
                            <div class="form-group">
                                <label class="pull-left">Move Furniture</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Address</label>
                        <div class="col-sm-10 col-form-label">
                            <div class="form-group">
                                <label class="pull-left">Some Address</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">No. of Movers</label>
                        <div class="col-sm-10 col-form-label">
                            <div class="form-group">
                                <label class="pull-left">3</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">No. of Trucks</label>
                        <div class="col-sm-10 col-form-label">
                            <div class="form-group">
                                <label class="pull-left">1</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-2 col-form-label">Job Requests</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <select class="form-control" name="Jobs">
                                    <option selected>Select Employee/Contractor</option>
                                    <option>Contractor 1</option>
                                    <option>Contractor 2</option>
                                    <option>Employee 1</option>
                                    <option>Contractor 3</option>
                                </select>
                                <span class="form-text">List of Job Requests.</span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Assign Job</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>