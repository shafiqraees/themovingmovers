<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('include.admin.head')

<body class="">
<div class="wrapper ">
    <div class="main-panel">
        @include('include.admin.header')
        @include('include.admin.sidebar')

        <div class="content">
            @yield('content')

            @yield('scripts')
        </div>

        @include('include.admin.footer')
    </div>
</div>
@include('include.admin.foot')
</body>

</html>
