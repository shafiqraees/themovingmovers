<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@include('include.user.head')

<body class="">
<div class="wrapper ">
    <div class="main-panel">
        @include('include.user.header')
        @include('include.user.sidebar')

        <div class="content">
            @yield('content')
        </div>

        @include('include.user.footer')
    </div>
</div>
@include('include.user.foot')
</body>

</html>
