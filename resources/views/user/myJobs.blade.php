@extends('layouts.user.app')

@section('title','My Job')
@section('heading','My Jobs')

@section('content')
    <div class="col-md-12">
        <div class="card">{{--
            <div class="card-header">
                <h4 class="card-title"> My Jobs</h4>
            </div>--}}
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                Order Title
                            </th>
                            <th>
                                Package
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                Time
                            </th>
                            <th>
                                Status
                            </th>
                        </tr></thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            <td>Some Job</td>
                            <td>Premium Service</td>
                            <td>01/01/2020 </td>
                            <td>10 AM to 1 PM</td>
                            <td>Pending</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>Move That</td>
                            <td>Economic Service</td>
                            <td>01/01/2020 </td>
                            <td>10 AM to 1 PM</td>
                            <td>Approved</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>Move this</td>
                            <td>Standard Service</td>
                            <td>01/01/2020 </td>
                            <td>10 AM to 1 PM</td>
                            <td>Rejected</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
