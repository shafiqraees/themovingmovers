@extends('layouts.user.app')

@section('title','Order List')
@section('heading','Orders')

@section('content')
    <style>/* basic positioning */
        .legend { list-style: none; }
        .legend li { float: left; margin-right: 10px; }
        .legend span { border: 1px solid #ccc; float: left; width: 12px; height: 12px; margin: 2px; }
        /* your colors */
        .legend .superawesome { background-color: #B9F0B6; }
        .legend .awesome { background-color: #F7D321; }
        .legend .kindaawesome { background-color: #FFE8C2; }
        .legend .notawesome { background-color: #FFC2C2; }</style>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <ul class="legend">
                    <li><span class="superawesome"></span>Open</li>
                    <li><span class="awesome"></span> Assigned</li>
                    <li><span class="kindaawesome"></span> Completed</li>
                    <li><span class="notawesome"></span> Cancelled</li>
                </ul>
                
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <div id="fullCalendar"></div>
                            <div id="data" style="display: none"></div>
                            <div id="packages" style="display: none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--<div class="col-md-12">
        <div class="card">--}}{{--
            <div class="card-header">
                <h4 class="card-title"> Order List</h4>
            </div>--}}{{--
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead class="text-primary">
                        <tr><th class="text-center">
                                #
                            </th>
                            <th>
                                Package
                            </th>
                            <th>
                                Date
                            </th>
                            <th class="text-center">
                                Time
                            </th>
                            <th class="text-right">
                                Duration
                            </th>
                        </tr></thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>--}}
@endsection
