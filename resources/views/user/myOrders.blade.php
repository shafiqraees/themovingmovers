@extends('layouts.user.app')

@section('title','Order List')
@section('heading','My Orders')

@section('content')
<div class="col-md-12">
    <div class="card">{{--
        <div class="card-header">
            <h4 class="card-title"> Order List</h4>
        </div>--}}
        <div class="card-body">
            <div class="table-responsive">
                <table class="table">
                    <thead class="text-primary">
                    <tr><th class="text-center">
                            #
                        </th>
                        <th>
                            Order Title
                        </th>
                        <th>
                            Package
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Time
                        </th>
                        <th>
                            Completed On
                        </th>
                    </tr></thead>
                    <tbody>

                    <tr>
                        <td>1</td>
                        <td>Move Furniture</td>
                        <td>Premium Service</td>
                        <td>01/01/2020 </td>
                        <td>10 AM to 1 PM</td>
                        <td>01/01/2020 </td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Move This</td>
                        <td>Standard Service</td>
                        <td>01/01/2020 </td>
                        <td>5 PM to 7 PM</td>
                        <td>01/01/2020 </td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Move That</td>
                        <td>Premium Service</td>
                        <td>01/01/2020 </td>
                        <td>6 PM to 8 PM</td>
                        <td>01/01/2020 </td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Some Title</td>
                        <td>Economic Service</td>
                        <td>01/01/2020 </td>
                        <td>10 AM to 12 PM</td>
                        <td>01/01/2020 </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
