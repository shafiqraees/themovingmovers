<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Package;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\DataTables;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $orders = Order::select('title',DB::raw("CONCAT(date,' ',startTime) AS start"),DB::raw("CONCAT(date,' ',endTime) AS end"),'address as className')->get()->toArray();
            $data = json_encode($orders);
            $packages = Package::all();
            return view('admin.order',compact('data','packages'));
        }
        catch ( \Exception $e){
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $packages = Package::all();
        return  view('admin.addOrder',compact('packages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $order = Order::create([
                'title' => $request->title,
                'date' => $request->date,
                'startTime' => $request->startTime,
                'endTime' => $request->endTime,
                'address' => $request->address,
                'user_id' => Auth::user()->id,
                'package_id' => $request->package,
            ]);
            //dd($order);
            DB::commit();
            $orders = Order::all();
            return redirect()->route('admin.orderHistory',compact('orders'));
        }
        catch ( \Exception $e) {
            DB::rollBack();
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function history(Request $request)
    {
        if ($request->ajax()) {
            $data = Order::with('package')->get();
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('package_name', function ($post) {
                    return $post->package->name;
                })

                ->rawColumns(['package_name'])
                ->make(true);
        }


        return view('admin.orderHistory');

    }
    public function test()
    {
        try{
            $orders = Order::select('id','title',DB::raw("CONCAT(date,' ',startTime) AS start"),DB::raw("CONCAT(date,' ',endTime) AS end"),'address as className')->get();

            $packages = Package::all();
            return view('admin.testOrder',compact('orders','packages'));
        }
        catch ( \Exception $e){
            return Redirect::back()->withErrors('Sorry something went wrong');
        }
    }
}
