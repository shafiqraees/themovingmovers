<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $user = [
            [
                'first_name' => 'Admin',
                'last_name' => 'Test',
                'email' => 'admin@admin.com',
                'is_admin' => '1',
                'password' => Hash::make('12345678'),
            ],
            [
                'first_name' => 'User',
                'last_name' => 'Test',
                'email' => 'user@user.com',
                'is_admin' => '0',
                'password' => Hash::make('12345678'),
            ],
        ];

        foreach ($user as $key => $value) {
            User::create($value);
        }
    }
}
