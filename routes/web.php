<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/user', [App\Http\Controllers\User\HomeController::class, 'index'])->name('user');
    Route::resource('user/order',App\Http\Controllers\User\OrderController::class);
    Route::get('user/myOrders',[App\Http\Controllers\User\OrderController::class,'create'])->name('myorders');
    Route::resource('user/myJobs',App\Http\Controllers\User\JobController::class);
    Route::resource('user/package',App\Http\Controllers\User\PackageController::class);


});

Route::group(['middleware' => ['isAdmin']], function () {
    Route::get('/admin', [App\Http\Controllers\Admin\HomeController::class, 'index'])->name('admin');

    Route::resource('admin/order',App\Http\Controllers\Admin\OrderController::class);
    Route::get('/admin/addOrder', [App\Http\Controllers\Admin\OrderController::class, 'create'])->name('addorder');
    Route::get('/admin/testOrder', [App\Http\Controllers\Admin\OrderController::class, 'test'])->name('test');
    Route::get('/admin/history', [App\Http\Controllers\Admin\OrderController::class, 'history'])->name('admin.orderHistory');


    Route::resource('admin/package',App\Http\Controllers\Admin\PackageController::class);

    Route::resource('admin/employee',App\Http\Controllers\Admin\EmployeeController::class);
    Route::resource('admin/jobOffer',App\Http\Controllers\Admin\JobController::class);
    Route::get("/admin/addPackage", function(){return view('admin.addPackage');});
    Route::get("/admin/addEmployee", function(){return view('admin.addEmployee');});
});




